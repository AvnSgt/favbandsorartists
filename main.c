#include <stdio.h>
int main(void) {
    //Multi dimensional array to store character strings.
    char favBands[3][30] = {"Cradle of Filth", 
                            "Jelly Roll", 
                            "Struggle Jennings"};
    /*Counter for seperated from "for loop," iterate list
      in printf function contained within loop*/
    int count = 0; 
    printf("Some of my favorite bands are: \n");
    /*For loop to iterate over multi dimensional array 
      and display output.*/
    for(int i = 0; i <=2; i++){
        count++;
        printf("%d: %s \n",count,favBands[i]);    
    }
    return 0;
}
